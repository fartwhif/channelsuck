import re
import time
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options as ChromeOptions
import undetected_chromedriver as uc


def ChannelsExtractFromHtml(html):
    links = []
    x = re.findall('title="([^"]+)" href="/([^"]+)">', html)

    for m in x:
        if (
            m[1].startswith("@")
            or m[1].startswith("c/")
            or m[1].startswith("channel/")
            or m[1].startswith("user/")
        ):
            if (  # filter out fake and other channels
                "UC4R8DWoMoI7CAwX8_LjQHig" in m[1]
                or "UCrpQ4p1Ql_hG8rKXIKM1MO" in m[1]  # Live
                or "UCtFRv9O2AHqOZjjynzrv-xg" in m[1]  # Fashion & Beauty
                or "UCEgdi0XIXXZ-qJOFPf4JSKw" in m[1]  # Learning
                or "UC-9-kyTW8ZkZNDHQJ6FgpwQ" in m[1]  # Sports
                or "UCYfdidRxbB8Qhf0Nx7ioOYw" in m[1]  # Music
                or "UClgRkhTL3_hImCAmdLfDE4g" in m[1]  # "News"  # Movies & Shows
                or "UCkYQyvc_i9hXEo4xic9Hh2g" in m[1]  # Shopping
                or "Your channel" in m[0]
            ):
                continue
            if not links.__contains__([m[0], m[1]]):
                links.append([m[0], m[1]])
    return links


def go(url, title):
    driver.get(url)
    waiting = True
    while waiting:
        time.sleep(0.1)
        docTitle = str(driver.execute_script("return document.title;"))
        if title in docTitle:
            waiting = False

    # print('page loaded, injecting jquery')
    driver.execute_script(
        'var headID = document.getElementsByTagName("head")[0];'
        + "var newScript = document.createElement('script');"
        + "newScript.type = 'text/javascript';"
        + "newScript.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js';"
        + "headID.appendChild(newScript);"
    )

    driver.execute_script(
        "document._x = (STR_XPATH) => {"
        + "    var xresult = document.evaluate(STR_XPATH, document, null, XPathResult.ANY_TYPE, null);"
        + "    var xnodes = [];"
        + "    var xres;"
        + "    while (xres = xresult.iterateNext()) {"
        + "        xnodes.push(xres);"
        + "    }"
        + "    return xnodes;"
        + "}"
    )
    WebDriverWait(driver, 10).until(
        lambda s: s.execute_script('return (typeof jQuery != "undefined")')
    )
    driver.execute_script("$.noConflict();")
    driver.execute_script("$ = jQuery;")
    # driver.execute_script('console.log($);')
    # print('jquery ready')
    return


def GetLinks():
    go("https://www.youtube.com/feed/subscriptions", "Subscriptions - YouTube")

    shelfOpen = str(
        driver.execute_script(
            "return $($(document.querySelector('#guide-button')).children()[0]).attr('aria-pressed');"
        )
    ).strip()
    if shelfOpen != "true":
        elem = driver.find_element(By.XPATH, '//*[@id="guide-icon"]')
        elem.click()

    driver.execute_script(
        "var showMoreBtn = jQuery('a[title*=\"Show \"]').filter(function() { return /Show \d+ more/.test($(this).text()); });"
        + "if (showMoreBtn.length > 0) { showMoreBtn.get(0).scrollIntoView(); } "
    )
    driver.execute_script(
        "var showMoreBtn = jQuery('a[title*=\"Show \"]').filter(function() { return /Show \d+ more/.test($(this).text()); });"
        + "if (showMoreBtn.length > 0) { showMoreBtn.parent().click(); } "
    )
    html = driver.execute_script("return document.documentElement.outerHTML;")
    links = ChannelsExtractFromHtml(html)
    with open("links.txt", "w") as file:
        for link in links:
            file.write(link[0] + "\t" + link[1] + "\n")
    return


def Subscribe():
    jsGetBtnTxt = "return $(document.querySelector(\"#subscribe-button-shape > button\")).attr('aria-label')"
    jsBtnClick = '$(document.querySelector("#subscribe-button-shape > button")).click()'
    # jsGetBtnTxt = "return $(document.querySelector(\"#subscribe-button > ytd-button-renderer > yt-button-shape > button\")).attr('aria-label')"
    # jsBtnClick = '$(document.querySelector("#subscribe-button > ytd-button-renderer > yt-button-shape > button")).click()'
    baseUrl = "https://www.youtube.com/feed/subscriptions"
    go(baseUrl, " - YouTube")  # for utility injection
    with open("links.txt", "r") as file:
        links = file.readlines()
    for link in links:
        _link = link.split("\t")
        driver.execute_script('$("title", "head").text("LOADING");')
        driver.execute_script('document.title = "LOADING";')
        go("https://www.youtube.com/" + _link[1].rstrip(), " - YouTube")
        time.sleep(pre_wait)
        btnTxt = str(driver.execute_script(jsGetBtnTxt)).strip()
        if btnTxt.upper().startswith("UNSUBSCRIBE"):
            continue
        driver.execute_script(jsBtnClick)
        time.sleep(post_wait)  # give it time to push the update
    return


def main():
    env = 1


if __name__ == "__main__":
    main()

options = ChromeOptions()
# options.add_argument("--headless")
options.headless = False
options.user_data_dir = "C:\\test\\data3"  # sometimes a special home for data is needed
driver = uc.Chrome(options)

# adjust these as needed
pre_wait = 1
post_wait = 1.3

# How to delete your YouTube channel and restore your subscriptions:
# 1. run it once and login to your google account, close browser
# 2. uncomment GetLinks()
# 3. run GetLinks()
# 4. copy links.txt somewhere safe just in case!
# 5. comment GetLinks() and uncomment Subscribe()
# 6. delete your YouTube channel
# 7. run Subscribe()
# 8. for long lists run Subscribe() again just in case

# GetLinks()  # reads your subs, writes to links.txt WARNING: overwrites links.txt
# Subscribe()  # reads from links.txt, all your subs, and subscribes to them

while True:
    time.sleep(1)
