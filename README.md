# channelsuck - backup and restore for your Youtube subscriptions:

I created this after learning that in order to "delete my channel" I had to loose all my subscriptions.  

Knowing how these people operate, I reckon this is probably a deliberately imposed disincentive against deleting your own channel, so I got to work and channelsuck was born.

## Prerequisite

run this in command line:

```
C:\projects\channelsuck>python -m venv env
C:\projects\channelsuck>"./env/scripts/activate.bat"
(env) C:\projects\channelsuck>pip install -r requirements.txt
(env) C:\projects\channelsuck>python -m channelsuck.py
```

## Operation
1. run it once and login to your google account, close browser
2. uncomment GetLinks()
3. run GetLinks()
4. copy links.txt somewhere safe just in case!
5. comment GetLinks() and uncomment Subscribe()
6. login with the account you want to restore the subscriptions to
7. uncomment and run Subscribe()
8. for long lists run Subscribe() again just in case

## Notes

I wasn't able to get chrome to remember my login after closing chrome and opening it again until i used a data dir, uncomment the line
```
# options.user_data_dir = "C:\\test\\data3"
```
by removing the pound sign # at the front of it, and also create that folder, to hold your user data

run channelsuck and login, close the browser, run channelsuck again and check that you're still logged in

